import React, {useState} from 'react';

import Producte from './Producte.jsx';
import Ticket from './Ticket.jsx';
import styled from 'styled-components';

import productes from './datos.json';

const Meitat = styled.div`
  width: 48%;
  display: inline-block;
`;


const App = () => {

  const [ compra, setCompra ] = useState([]);

  const afegir = (item) => {
    
    const actual = compra.find(el => el.id===item.id);
    if (actual) {
      const nou = {...actual};
      nou.units += 1;
      setCompra([...compra.filter(el => el.id!==nou.id), nou]);
    } else {
      const nou = item;
      nou.units=1 ;
      setCompra([...compra, nou]);
    }
   
  }

  const eliminar = (item) => {
    setCompra(compra.filter(el => el.id!==item.id))
  }


  return (
    <>
      <Meitat>
        {productes.map(el => <Producte item={el} afegir={afegir} />)}
      </Meitat>
      <Meitat>
        <Ticket dades={compra} eliminar={eliminar} />
      </Meitat>
    </>
  );
}

export default App;
