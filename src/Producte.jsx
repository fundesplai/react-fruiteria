import styled from 'styled-components';


const DivProducto = styled.div`
    border: 1px solid black;
    padding: 20px;
`;

export default  ({afegir, item}) => {
    return (
      <DivProducto>
        <span>{item.nom}</span>
        <span>{item.preu}</span>
        <button onClick={()=>afegir(item)} >afegir</button>
      </DivProducto>
    )
  }