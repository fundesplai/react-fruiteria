import styled from "styled-components";

const DivCompra = styled.div`
  border: 1px solid black;
  padding: 20px;
`;

const Compra = ({ eliminar, item }) => {
  return (
    <DivCompra>
      {item.nom}
      <br />
      {item.preu}
      <br />
      {item.units}
      <br />
      <button onClick={() => eliminar(item)}>eliminar</button>
    </DivCompra>
  );
};

export default ({ dades, eliminar }) => {
  const coses =
    dades &&
    dades.map((el) => <Compra key={el.id} item={el} eliminar={eliminar} />);

let  total = 0;
dades.forEach(el => total = total + el.preu*el.units);
  return (
    <>
      <h1>coses...</h1>
      {coses}
      <h5>Total {total}</h5>
    </>
  );
};
